package aec.schedule_provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import model.Weather;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.DataInput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RefreshScope
@RequestMapping("/api")
public class Controller {
    private final RestTemplate restTemplate;

    @Autowired
    Environment env;

    @Autowired
    ServiceImplementation serviceImplementation;

    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    public Controller(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Value("${target.week:2}")
    String targetWeek;

    @Value("${target.threshold}")
    String threshold;

    @RequestMapping("/")
    public String home(){
        return "Hello from Schedule Service running at port: " + env.getProperty("local.server.port");
    }

    @HystrixCommand(fallbackMethod = "fallback")
    @GetMapping("/weekly")
    public List getWeeklySchedule(){
        LOGGER.info("Creating list object ... ");
        List lessons = restTemplate.getForObject("http://db-service/api/test?weekN=" + targetWeek, List.class);
        //TODO - достать прогноз на день из конфиг файла - завтра
        //TODO - добавить прогонз в возвращаемый набор объектов
        LOGGER.info("Returning list ... ");
        return lessons;
    }

    // a fallback method to be called if failure happened
    public List<Object> fallback(Throwable hystrixCommand){
        List<Object> res = new ArrayList<>();
        res.add("Circuit breaker triggered");
        return res;
    }

    @GetMapping("/yandex")
    public ResponseEntity<String> getWeather() throws IOException, JSONException {
        ResponseEntity<String> res = serviceImplementation.getWeeklyWeather();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(res.toString());
        Weather weather = mapper.readValue(res.toString().substring(5), Weather.class);
        System.out.println(weather.toString());
        return res;
    }

    @GetMapping("/tomorrowTemp")
    public String getTomorrowWeather() throws IOException, JSONException {
        ResponseEntity<String> res = serviceImplementation.getWeeklyWeather();
        JSONObject json = new JSONObject(res.toString().substring(5)); //полный json
        JSONArray jsonarr = json.getJSONArray("forecasts"); //прогнозы
        json = jsonarr.getJSONObject(1);    //прогноз на нужный день
        String date = json.getString("date");
        json = json.getJSONObject("parts"); //время суток
        json = json.getJSONObject("morning"); //утро
        int temp = Integer.parseInt( json.getString("temp_min"));
        String recommendation;
        if(temp < Integer.parseInt(threshold)){
            recommendation = "Завтра ("+date+") холодно ("+temp+"), можно договориться об отмене пар";
        } else {
            recommendation = "Завтра ("+date+") Тепло ("+temp+"), пары будут";
        }
        return recommendation;
    }


}
