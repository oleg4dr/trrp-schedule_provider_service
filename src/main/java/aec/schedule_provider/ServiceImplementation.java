package aec.schedule_provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Base64;

@Service
public class ServiceImplementation {


    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
    //подцепим переменные из конфиг сервера
    @Value("${target.lat}")
    String lat;
    @Value("${target.lon}")
    String lon;
    @Value("${target.token}")
    String token;



    private HttpHeaders createHttpHeaders()
    {
        HttpHeaders headers = new HttpHeaders();
        //установим токен доступа из конфиг файла в заголовок и объявим ожидаемый медиа формат ответа
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("X-Yandex-API-Key", token);
        return headers;
    }

    ResponseEntity<String> getWeeklyWeather(){
        //возьмем из конфиг файла широту и долготу для запроса
        String theUrl = "https://api.weather.yandex.ru/v1/forecast?lat=" + lat + "&lon=" + lon;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHttpHeaders();
        RequestEntity requestEntity = new RequestEntity(headers, HttpMethod.GET, URI.create(theUrl));
        return restTemplate.exchange( requestEntity, String.class);
    }
}
