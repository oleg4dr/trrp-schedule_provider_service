package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather implements Serializable {
    private String now;
    @JsonProperty("now_dt")
    private String now_dt;
    private Integer pressure;
    private String date;
    //достанем 1 свойство из вложенного объекта
    @SuppressWarnings("unchecked")
    @JsonProperty("info")
    private void unpackPressure(Map<String,Object> pressureVal) {
        this.pressure = (Integer) pressureVal.get("def_pressure_mm");
    }


    @Override
    public String toString() {
        return "Weather{" +
                "now='" + now + '\'' +
                ", now_dt='" + now_dt + '\'' +
                ", pressure='" + pressure + '\'' +
                '}';
    }
}
